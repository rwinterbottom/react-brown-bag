class ButtonWithPurpose extends React.Component {
	render () {
		return (
			<button onClick={this.props.onClick}>{this.props.label}</button>
		)
	}
}

class Counter extends React.Component {
	constructor (props) {
		super(props);
		
		this.state = { count: 0 };
		this.decrement = this.decrement.bind(this);
	}
	
	increment = () => {
		this.setState((prevState, props) => {
			return {
				count: prevState.count + 1
			};
		});
	};
	
	decrement () {
		this.setState((prevState, props) => {
			return {
				count: prevState.count - 1
			};
		});
		
		// Another version of above
		// this.setState({ count: this.state.count - 1 });
	}
	
	render () {
		return (
			<div className='counter'>
				<ButtonWithPurpose onClick={this.increment} label='Increment' />
				{this.state.count}
				<ButtonWithPurpose onClick={this.decrement} label='Decrement' />
			</div>
		);
	}
}

let PureCounter = (props) => {
	return (
		<div className='counter'>
			<ButtonWithPurpose onClick={props.increment} label='Increment' />
			{props.count}
			<ButtonWithPurpose onClick={props.decrement} label='Decrement' />
		</div>
	);
};

class App extends React.Component {
	
	constructor (props) {
		super(props);
	
		this.state = { count: 0 };
	}
	
	increment = () => {
		this.setState((prevState, props) => {
			return {
				count: prevState.count + 1
			};
		});
	};
	
	decrement = () => {
		this.setState((prevState, props) => {
			return {
				count: prevState.count - 1
			};
		});
	};
	
	render () {
		let { name } = this.props;

		return (
			<React.Fragment>
				<h1>Hello {name}</h1>
				<h2>This is a simple counter example</h2>
				<PureCounter
					count={this.state.count}
					increment={this.increment}
					decrement={this.decrement}
				/>
				<PureCounter
					count={this.state.count}
					increment={this.increment}
					decrement={this.decrement}
				/>
			</React.Fragment>
		);
	}
	
}

ReactDOM.render(
	<App name='World' />,
	document.getElementById('root')
);

