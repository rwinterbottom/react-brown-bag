React & Redux Demo
===========================

## [React](https://reactjs.org/)

### Mounting to the DOM

Starting a React application is really easy. Just mount your root component to the dom.

```javascript
import App from 'components/App';
import ReactDom from 'react-dom';
import React from 'react';

ReactDom.render(
	<App />,
	document.getElementById('react-root')
);

```

### JSX

JSX is not required but can be easier to develop. It looks like html but has some subtle nuances.

The JSX way:
```javascript
<div>
	<MyComponent />
	<User user={someUserVariable} />
	<Counter count={currentCount} />
</div>
```

The non-JSX way:
```javascript
class MyComponent extends React.Component {
	render () {
		React.createElement('div', null, `Hello ${this.props.greeting}`);
	}
}

ReactDom.render(
	React.createElement(MyComponent, { greeting: 'world'}, null),
	document.getElementById('react-root')
);
```

#### Escaping to Javascript

It's really easy to execute any javascript in your template.

```javascript
// Embedding a variable
let greeting = 'friend';
<h1>Hello {greeting}!</h1>
// Mapping over a list
let items = [
	{ id: 1, text: 'foo'},
	{ id: 2, text: 'bar'}
];
<div>
	{items.map(item =>
		<ListItem id={item.id} text={item.text} />
	)}
</div>
```

#### Caveats

JSX lives in javascript files so there are some naming collisions you need to be aware of. For example, css classes are called `className` and `for` on labels is `htmlFor`.

```javascript
<div className='my-css-class' />
<label htmlFor='foo' />
```

See [DOM Elements](https://reactjs.org/docs/dom-elements.html) for more examples and more explanations.

### Components

React has several ways to create components. The framework is a component based framework so knowing what all the options are and how and when to use them is very important. There are also many design patterns around composing components, extending components, controlled components, using higher order components, and lately, using render props to reuse component functionality. See the advanced guides of the [React Docs](https://reactjs.org/docs/hello-world.html) for details.

#### Class Components

Class components are among the most common and typically look something like this.

```javascript
import React from 'react';

class MyComponent extends React.Component {
	constructor (props) {
		super(props);
		// You can set default state here or do other setup related things
		this.state = props.defaultState;
		// You should not make API calls here, do that in componentDidMount
	}
	
	// You can add as many lifecycle methods here as you want
	render () {
		return <div>Yayyy</div>;
	}
}
```

#### Functional Components

These are a shorthand way of building components. These do not have local state and typically do not have lifecycle methods.

```javascript
function MyComponent (props) {
	return <div>Hello {props.greeting}</div>;
}
```

#### Pure Components

Pure components are components that have no side-effects. They only render data from props and those props cannot mutate. The one rule is it must render the exact same output when given the same state and props. The benefit to a pure component is that it will automatically do a shallow compare on props and state in `shouldComponentUpdate` to determine if it needs to re-render. This means if you have deeply nested JSON, you may get false positives unless your using immutable objects. Also, since it skip's rendering, any child components must also be pure.

```javascript
// Simple Example
class Counter extends React.PureComponent {
	constructor (props) {
		super(props);
		this.state = { count: 1 };
	}
	render () {
		return (
			<button
				onClick={() => this.setState((currentState) => ({ count: currentState.count + 1 })) }>
				Count: { this.state.count }
			</button>
		);
	}
}

// Complex Example
class ReallyBigList extends React.PureComponent {
	render () {
		// List items should be an immutable object, shouldComponentUpdate will check
		// reference equality on it
		let { list_items } = this.props;
		
		return (
			/**
			* This will render lot's of list items ListItem
			* needs to also be a pure component. If this.props.list_items
			* is immutable, then the shallow compare will stop this list and
			* every single list item from 're-rendering' in the virtual dom and
			* greatly increase performance since none of this part of the tree is diffed
			*/
			<div className='my_optimized_list'>
				{list_items.map(item => <ListItem item={item} />)}
			</div>
		);
	}
}
```

### Lifecycle

Components have lifecycle methods that you can hook into. These can give you hooks to run some initialization code, cleanup code, performance tweaks, and even error handling. All the current lifecycle methods are listed below:

```javascript
class LifeCycleExample extends React.Component {
	// These first four are called when the component is being inserted into the dom.
	// TODO: Verify this is the correct order in which the lifecycle methods are executed.
	
	// You can initialize state and bind methods here, make sure to call super(props) first.
	// Avoid introducing side-effects here or subscriptions, use componentDidMount.
	constructor (props) { super(props); }
	
	// Only lifecycle event called on server rendering. Called just before the component mounts.
	// You can call setState synchronously here and it wont trigger extra rendering.
	// Avoid introducing side-effects here or subscriptions, use componentDidMount.
	componentWillMount () {}
	
	// This method returns the html output of your component.
	// It runs on mount and any state/props change.
	render () {}
	
	// Invoked immediately after a component is mounted to the dom. If you need to load data
	// from an API, this is the place to do it. Also, set up subscriptions here.
	// You can call setState here and it will trigger an extra rendering, but it will happen
	// before the user see's it in the browser, so they won't see intermediate state.
	componentDidMount () {}
	
	// Updates are triggered by state changes or new props, the following lifecycle methods get
	// called in those cases. render also get's called when there are updates.
	
	// Invoked when a mounted component is about to receive new props. If you need to update
	// state in response to prop changes, this is the place to do it. This will get called even
	// if props don't change (parent props or state changed), so you may want to diff it.
	componentWillReceiveProps (nextProps) {}
	
	// This is for performance and returns a boolean. True means this component should re-render
	// false means nothing here or in this component's children changed, so skip rendering. Don't
	// overuse this as overusing could make your code actually slower. Best used with immutable objects
	// and at key points in your component tree.
	shouldComponentUpdate (nextProps, nextState) {}
	
	// Invoked just before a component updates, you can perform preparation before an update occurs.
	// Do not call setState or do anything that would trigger a render in here.
	// Not invoked on initial render
	componentWillUpdate (nextProps, nextState) {}
	
	// render get's called again here
	
	// Invoked immediately after a component updates. You can perform dom updates here
	// or make network requests if you want (check state/props to make sure the request is necessary).
	// Not invoked on initial render
	componentDidUpdate (prevProps, prevState) {}
	
	// This method get's called when your component is about to unmount from the dom.
	// This is where cleanup should take place, disconnect events or cancel requests.
	componentWillUnmount () {}
	
	// This method catches errors during rendering, lifecycle methods, or in
	// the constructor of any child component. You can create an error boundary component
	// and use it to catch any errors and show appropriate error messages.
	componentDidCatch (error, info) {}
	
}
```

### Props and State

#### State
State is the state of your applications. Where you put it and how you use it depends on your application. If state is passed into another component, it becomes that component's props. Three things you should know about state are:

* Never modify state directly, use `setState(Object: newState)` or `setState(Function: updater)`. It should only be assigned in the constructor of a component.
* State updates may be asynchronous because they may be batched
```javascript
// In this example, when the count is logged
// it will not be count + 1 but instead be the
// original count. This method is often ok if
// you don't need the updated count until the next render
this.setState({ count: this.state.count + 1 });
console.log(this.state.count);
// You can use this as an example, if you need the updated
// count immediately after calling setState
this.setState((prevState, props) => {
	return { count: prevState + 1 };
});
```
* State updates are merged
```javascript
this.state = { count: 0, items: [] };
// Later when updating state, you do not need to update all the
// state at once, just the part you want, so this is fine.
this.setState({ count: 1 });
```

#### Props
Props should be treated as READ-ONLY. You should never modify props. If you can build components to only operate on props given to them, then they are essentially pure components and have several other benefits as well.

### Things to consider

#### Pros
* Performance (Server-side rendering, Virtual Dom, shouldComponentUpdate)
* Developer experience
* Developer tools
* Massive community
* Third-party libraries and facebook tools (create-react-app, material, etc.)
* Easy to reason about

#### Cons
* It's just the V in MVC
	* You can use local state but with large applications this quickly becomes unmanageable
	* Once your outside hello world, you typically need Webpack & Babel for compilation, Redux for state management, React Router if you want a SPA, and maybe other tools depending on what you want like things for service workers and other PWA enhancements.
* Different way of thinking. It is best used in a functional style but if your used to observables and MVC, this may be a bit of a shock. If your used to Angular 2 or above, the component style will look really similar.
* No defined or enforced architecture. React will enforce some rules, but nothing related to architecture.

#### Other
* [Create React App](https://github.com/facebook/create-react-app)
* [Lists and Keys](https://reactjs.org/docs/lists-and-keys.html)
* [Forms](https://reactjs.org/docs/forms.html)
* [Conditional Rendering](https://reactjs.org/docs/conditional-rendering.html)
* [Thinking in React](https://reactjs.org/docs/thinking-in-react.html)
* Look at the advanced guide which starts with [JSX in Depth](https://reactjs.org/docs/jsx-in-depth.html)

## [Redux](https://redux.js.org/)

### Important Concepts

* Actions all have a specific format and should conform to the same model.
* Every change for the application needs to be described as an action. This gives a very clear understanding of what's going on.
* Actions and state are tied together with reducers. The reducer takes the current state and an action and then returns the next state. These are typically broken into many smaller functions to make management much easier.
* Based on Flux, Facebook's own data model they use for most of their applications, which revolves around a strict unidirectional data flow.
* Redux works really well if you follow three core concepts:
	* Single source of truth
	* State is read only
	* Changes are made with pure reducers(pure functions basically)

### Actions

Actions describe changes in your application state and should all generally follow a similar structure. This structure is entirely up to you, but the general convention is to use the following:

```javascript
{
	// required
	type: "string", // What the action type is, reducers need this so they know whether or not to act on it
	// optional
	payload: "any", // contains the meat of the action, could be text, JSON, error, etc.
	// optional
	error: "boolean", // some people like to have a separate property for an error indicating one exists in the payload, totally up to the developer
	// optional
	meta: "any" // Can contain info about analytics or anything else, useful for middleware
}
```

Redux uses action creators to describe the action. A simple example might be something like this:

```javascript
function addTodo (todo_text) {
	return {
		type: 'ADD_TODO',
		payload: todo_text
	};
}
```

These are used in conjunction with a redux store, see below, by invoking the `store.dispatch` method like so:

```javascript
store.dispatch(addTodo('Give a demo on Redux'));
```

### Reducers

Reducers in redux take two arguments, the current state and the action (similar to the `reduce` function). Their sole purpose is to look at the action type, if it is one they know how to handle, return a new state, otherwise return the current state. For example, if we had a reducer for our `addTodo` action from above, it may look something like this:

```javascript
function todoAppReducer (state = initialState, action) {
	switch (action.type) {
		case 'ADD_TODO':
			// Don't modify current state, return a new state so your React app can easily
			// track state changes and be more efficient (e.g. using PureComponent)
			return Object.assign({}, state, {
				todos: state.todos.concat({
					text: action.payload,
					completed: false,
					id: generate_uuid()
				})
			});
		default:
			return state;
	}
}
```

Reducers can be broken into many small reducer functions.  You can then use Redux's `combineReducers` helper to generate a big reducer (for your whole application), which makes it really clean. See [Splitting Reducers](https://redux.js.org/basics/reducers#splitting-reducers).

### Store

The store contains your application state and has a couple of important functions to know how to use which are outlined in the snippet below.

```javascript
// Get current state with `getState`
store.getState();
// Subscribe to state changes with `subscribe`, this takes a callback to invoke
// when state has changed and also returns a handle to use for unsubscribing
let unsubscribe = store.subscribe(function () {
	// Get updated state in here with getState again
	store.getState();
});
// dispatch actions to update the state with `dispatch`
store.dispatch(addTodo('Learn about Redux'));
store.dispatch(completeTodo({ id: 2 }));
// call unsubscribe later to stop receiving updates
unsubscribe();
```

The way to create a store is to give it your root reducer.

```javascript
// appStore.js
import todoAppReducer from './todoAppReducer';
import { createStore } from 'redux';

export default createStore(todoAppReducer);

// everywhere else
import store from './appStore';
```

#### Middleware

Middleware is a slightly more advanced yet really useful feature. You can give your store middleware which intercepts all actions before they get sent to the reducers. You can log all of your actions, create support for async action creators that return promises instead of JSON, and even report out to some Analytics endpoint all from a single middleware function. This would not require any changes to your application code either which makes for a very clean way to add functionality across the whole app.

## Simplest of examples

Here is a simplified counter example based on the example from [redux's website](https://redux.js.org/introduction/examples#counter):

```javascript
import { createStore } from 'redux';
// Here are some action creators
function increment () {
	return { type: 'INCREMENT' };
}

function decrement () {
	return { type: 'DECREMENT' };
}

// This is the root reducer
function counterReducer (state = 0, action) {
	switch (action.type) {
		case 'INCREMENT':
			return state + 1;
		case 'DECREMENT':
			return state - 1;
		// Always have a default and return the current state
		default:
			return state;
	}
}

const store = createStore(counterReducer);

const unsubscribe = store.subscribe(function () {
	const state = store.getState();
	// unsubscribe when the count hits three
	if (state > 2) {
		unsubscribe();
	}
});

store.dispatch(increment()); // state = 1
store.dispatch(increment()); // state = 2
store.dispatch(decrement()); // state = 1
store.dispatch(increment()); // state = 2
store.dispatch(increment()); // state = 3
// We have now unsubscribed from the store and no longer receive updates
```

#### Other
* [Advanced Concepts](https://redux.js.org/advanced)
