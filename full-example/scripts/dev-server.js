const WebpackDevServer = require('webpack-dev-server');
const make_config = require('../webpack.config');
const webpack = require('webpack');

// Set the NODE_ENV to development
process.env.NODE_ENV = 'development';

// Setup our webpack config
let webpack_config = make_config();

// Create our compiler needed for the server
let compiler = webpack(webpack_config);

// DevServer settings are ignored when using the Node API, we need to pass them in
let server = new WebpackDevServer(compiler, webpack_config.devServer);

// Start the server
let port = process.env.PORT || 8080;
server.listen(port, '127.0.0.1', () => {
	console.log(`\x1B[1mStarting server on \x1B[36mhttp://localhost:${port}\x1B[39m\x1B[22m`);
});
