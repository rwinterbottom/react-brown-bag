const make_config = require('../webpack.config');
const webpack = require('webpack');
const path = require('path');

const root = process.cwd();

let index_entry = path.join(root, 'src/js/components/App/index');
let output_path = path.join(root, 'dist');
let webpack_config = make_config({ precompile: true });

// Override the entry
webpack_config.entry = {
	app: index_entry
};

// Override the output
webpack_config.output = {
	path: output_path,
	filename: 'js/app.compiled.js',
	libraryTarget: 'commonjs2'
};

let compiler = webpack(webpack_config);

console.log('\x1B[1mStarting pre-compile script\x1B[22m');
console.log('---------------------');

compiler.run((err, stats) => {

	if (err) { throw err; }

	// Clear process.stdout
	process.stdout.clearLine();
	process.stdout.cursorTo(0);

	// Output some stats
	console.log(stats.toString({
		errorDetails: false,
		warnings: false,
		modules: false,
		chunks: false,
		colors: true
	}));

	console.log();
	console.log('\x1B[1mPrecompiling has completed\x1B[22m');
	console.log('------------------------------');
	console.log();

});
