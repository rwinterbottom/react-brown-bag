/**
 * @description This is a custom webpack plugin that can take css
 * extracted by ExtractTextPlugin and inject it via HtmlWebpackPlugin.
 * The result is certain css can be inlined into your html templates.
 * @requires https://github.com/webpack-contrib/extract-text-webpack-plugin
 * @requires https://github.com/jantimon/html-webpack-plugin
 * @param {object} options
 * @param {string} options.name - Property name to use when setting on the html plugin data
 * @param {string} options.styles_path - Path to css to inline
 * @example Use the following as an example on how to set this up:
 * In webpack.config.js, under your plugins
 * plugins: [
 *	 new ExtractTextPlugin('css/critical.css'),
 *	 new InlineStylePlugin({ name: 'critical_css', styles_path: 'css/critical.css' })
 *	 new HtmlWebpackPlugin({ ... })
 * ]
 * In your html somewhere:
 * <style><%= htmlWebpackPlugin.options.critical_css %></style>
 */
class InlineStylePlugin {
	constructor (options = {}) {
		// Check against required arguments
		if (!options.name) {
			throw new Error('Missing `name` option. You must provide a name to use for the output.');
		}

		if (!options.styles_path) {
			throw new Error('Missing `styles_path` option. You must provide a path to a css file.');
		}

		this.name = options.name;
		this.styles_path = options.styles_path;
	}
	
	apply (compiler) {
		let name = this.name;
		let styles_path = this.styles_path;
		// Hook a callback into webpack's compilation step
		compiler.plugin('compilation', function (compilation) {
			// Hook into HtmlWebpackPlugin's event that fires before html is processed
			// See https://github.com/jantimon/html-webpack-plugin#events
			compilation.plugin('html-webpack-plugin-before-html-generation', function (plugin_data, callback) {
				let asset = compilation.assets[styles_path];
				if (asset) {
					plugin_data.plugin.options[name] = asset.source();
				}
				callback(null, plugin_data);
			});
		});
	}
}


module.exports = InlineStylePlugin;
