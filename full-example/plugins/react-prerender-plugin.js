const { renderToString } = require('react-dom/server');
const { createElement } = require('react');

/**
 * @description This is a custom webpack plugin that can take a pre-compiled
 * component and inject it via HtmlWebpackPlugin into your html.
 * @requires https://github.com/jantimon/html-webpack-plugin
 * @param {object} options
 * @param {string} options.name - Property name to store the markup on in htmlWebpackPlugin options
 * @param {string} options.component_path - Path to your compiled component
 * @example Use the following as an example on how to set this up:
 * In webpack.config.js, under your plugins
 * plugins: [
 *	 new ReactPrerenderPlugin({ name: 'markup', component_path: path.resolve('./dist/js/app.compiled') }),
 *	 new HtmlWebpackPlugin({ ... })
 * ]
 * In your html somewhere:
 * <div id='react-root'><%= htmlWebpackPlugin.options.markup %></div>
 */
class ReactPrerenderPlugin {
	constructor (options = {}) {
		// Check against required arguments
		if (!options.name) {
			throw new Error('Missing `name` option. You must provide a name to use for the output.');
		}

		if (!options.component_path) {
			throw new Error('Missing `component_path` option. You must provide a component_path to use for the output.');
		}

		this.name = options.name;
		this.component_path = options.component_path;
	}
	
	apply (compiler) {
		let name = this.name;
		let component_path = this.component_path;
		// Hook a callback into webpack's compilation step
		compiler.plugin('compilation', function (compilation) {
			// Hook into HtmlWebpackPlugin's event that fires before html is processed
			// See https://github.com/jantimon/html-webpack-plugin#events
			compilation.plugin('html-webpack-plugin-before-html-generation', function (plugin_data, callback) {
				let component;
				// Try to load our component and if not, throw a more descriptive error
				try {
					component = require(component_path);
				} catch (err) {
					throw new Error('Unable to require pre-compiled component. Is component_path incorrect or did you forget to pre-compile.');
				}

				if (component) {
					// let default_props = { header: 'Server Rendered Todo' };
					// let markup = renderToString(createElement(component.default, default_props));
					let markup = renderToString(createElement(component.default))
					plugin_data.plugin.options[name] = markup;
					// plugin_data.plugin.options.INITIAL_STATE = JSON.stringify(default_props);
				}
				callback(null, plugin_data);
			});
		});
	}
}

module.exports = ReactPrerenderPlugin;
