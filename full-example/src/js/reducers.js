import { filterReducer } from './components/TodoStatusBar/reducers';
import { todoListReducers } from './components/App/reducers';
import { combineReducers } from 'redux';

// This is where we define our application state structure
export default combineReducers({
	current_filter: filterReducer,
	todo_list: todoListReducers
});
