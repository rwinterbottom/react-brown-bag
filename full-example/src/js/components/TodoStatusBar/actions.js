import { CHANGE_FILTER, CLEAR_ALL_COMPLETED_TODOS } from './constants';

export function changeCurrentFilter (new_filter) {
	return {
		type: CHANGE_FILTER,
		payload: new_filter
	};
}

export function clearAllCompletedTodos () {
	return {
		type: CLEAR_ALL_COMPLETED_TODOS
	};
}
