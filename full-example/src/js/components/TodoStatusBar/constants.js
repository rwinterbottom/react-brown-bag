// Action Constants
export const CHANGE_FILTER = 'CHANGE_FILTER';
export const CLEAR_ALL_COMPLETED_TODOS = 'CLEAR_ALL_COMPLETED_TODOS';

// Available Filters
export const FILTER_ALL = 'FILTER_ALL';
export const FILTER_NEW = 'FILTER_NEW';
export const FILTER_COMPLETED = 'FILTER_COMPLETED';
