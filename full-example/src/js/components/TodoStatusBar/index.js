import { changeCurrentFilter, clearAllCompletedTodos } from './actions';
import {
	FILTER_ALL,
	FILTER_NEW,
	FILTER_COMPLETED,
} from './constants';

import store from 'js/store';
import React from 'react';

export default class TodoStatusBar extends React.Component {
	
	printRemainingCount = (count) => {
		return count === 1
			? `${count} item remaining`
			: `${count} items remaining`;
	};
	
	clearCompletedTodos = () => {
		store.dispatch(clearAllCompletedTodos());
	};

	changeFilter = ({ target }) => {
		let filter = target.getAttribute('data-filter');
		store.dispatch(changeCurrentFilter(filter));
	};

	render () {
		return (
			<div className='todo-status-bar flex-row'>
				<div className='todo-status-bar__remaining-count corner-item'>
					{this.printRemainingCount(this.props.count)}
				</div>
				<div className='todo-status-bar__filters flex'>
					<button
						className='todo-status-bar__filters__button shadow pointer'
						onClick={this.changeFilter}
						data-filter={FILTER_ALL}>
						All
					</button>
					<button
						className='todo-status-bar__filters__button shadow pointer'
						onClick={this.changeFilter}
						data-filter={FILTER_NEW}>
						New
					</button>
					<button
						className='todo-status-bar__filters__button shadow pointer'
						onClick={this.changeFilter}
						data-filter={FILTER_COMPLETED}>
						Completed
					</button>
				</div>
				<button
					className='todo-status-bar__clear-completed corner-item shadow pointer'
					onClick={this.clearCompletedTodos}>
					Clear completed
				</button>
			</div>
		);
	}

}
