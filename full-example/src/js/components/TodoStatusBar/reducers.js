import { CHANGE_FILTER, FILTER_ALL } from './constants';

export function filterReducer (current_filter = FILTER_ALL, action) {
	switch (action.type) {
		case CHANGE_FILTER:
			return action.payload;
		default:
			return current_filter;
	}
}

export function clearAllCompletedReducer (todo_list = []) {
	return todo_list
		.filter(todo => !todo.completed);
}
