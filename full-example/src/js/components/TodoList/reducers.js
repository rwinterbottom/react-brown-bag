export function toggleTodoReducer (todo_list, action) {
	// create a copy, don't mutate
	return todo_list
		.map(todo => {
			if (todo.id === action.payload) {
				return Object.assign({}, todo, { completed: !todo.completed });
			}
			return todo;
		});
}

export function deleteTodoReducer (todo_list, action) {
	// create a copy, don't mutate
	return todo_list.filter(todo => todo.id !== action.payload);
}

export function editTodoReducer (todo_list, action) {
	// create a copy, don't mutate
	return todo_list;
}