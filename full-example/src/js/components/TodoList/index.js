import store from 'js/store';
import React from 'react';

import {
	FILTER_COMPLETED,
	FILTER_NEW,
	FILTER_ALL
} from '../TodoStatusBar/constants';

import {
	editTodo,
	deleteTodo,
	toggleTodo
} from './actions';

// Helper function to determine whether or not the todo should be showed
let shouldShowTodo = (todo, current_filter) => {
	switch (current_filter) {
		case FILTER_COMPLETED:
			return todo.completed;
		case FILTER_NEW:
			return !todo.completed;
		default:
			// Filter is filter_all which means show all todos
			return true;
	}
};

// List item for my todo list
let TodoListItem = ({ todo }) => (
	<div className={`todo-list__item flex-row ${todo.completed ? 'complete' : ''}`}>
		<div className='svg-icon-container flex pointer' onClick={() => store.dispatch(toggleTodo(todo.id))}>
			<div className='todo-list__item-icon__circle flex'>
				<svg className='svg-icon icon-checkmark'>
					<use xlinkHref='#icon-checkmark' />
				</svg>
			</div>
		</div>
		<input className='todo-list__item-input' defaultValue={todo.text} />
		<div className='svg-icon-container flex pointer' onClick={() => store.dispatch(deleteTodo(todo.id))}>
			<svg className='svg-icon icon-close'>
				<use xlinkHref='#icon-close' />
			</svg>
		</div>
	</div>
);

export default class TodoList extends React.Component {

	render () {
		let { todo_list, current_filter } = this.props;
		let items = todo_list
			.filter(todo => shouldShowTodo(todo, current_filter))
			.map(todo => <TodoListItem key={todo.id} todo={todo} />);
		
		return (
			<div className='todo-list flex'>
				{items}
			</div>
		);
	}

}
