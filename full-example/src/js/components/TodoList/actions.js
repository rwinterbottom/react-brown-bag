import { TOGGLE_TODO, EDIT_TODO, DELETE_TODO } from './constants';

export function editTodo (text) {
	return {
		type: EDIT_TODO,
		payload: text
	};
}

export function deleteTodo (id) {
	return {
		type: DELETE_TODO,
		payload: id
	};
}

export function toggleTodo (id) {
	return {
		type: TOGGLE_TODO,
		payload: id
	};
}
