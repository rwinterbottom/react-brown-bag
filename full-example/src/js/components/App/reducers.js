import { toggleTodoReducer, deleteTodoReducer, editTodoReducer } from '../TodoList/reducers';
import { addTodoReducer, toggleAllTodoReducer } from '../TodoInput/reducers';
import { clearAllCompletedReducer } from '../TodoStatusBar/reducers';

import { TOGGLE_TODO, DELETE_TODO, EDIT_TODO } from '../TodoList/constants';
import { CLEAR_ALL_COMPLETED_TODOS } from '../TodoStatusBar/constants';
import { ADD_TODO, TOGGLE_ALL_TODOS } from '../TodoInput/constants';

// Every reducer assigned here needs to return an array of todo items
export function todoListReducers (todo_list = [], action) {
	switch (action.type) {
		case TOGGLE_ALL_TODOS:
			return toggleAllTodoReducer(todo_list, action);
		case ADD_TODO:
			return addTodoReducer(todo_list, action);
		case TOGGLE_TODO:
			return toggleTodoReducer(todo_list, action);
		case DELETE_TODO:
			return deleteTodoReducer(todo_list, action);
		case EDIT_TODO:
			return editTodoReducer(todo_list, action);
		case CLEAR_ALL_COMPLETED_TODOS:
			return clearAllCompletedReducer(todo_list, action);
		default:
			return todo_list;
	}
}
