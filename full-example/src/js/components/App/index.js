import TodoStatusBar from 'js/components/TodoStatusBar';
import TodoInput from 'js/components/TodoInput';
import TodoList from 'js/components/TodoList';
import store from 'js/store';
import React from 'react';

// Load our non-critical style lazily
import 'css/app.scss';

export default class App extends React.Component {

	constructor (props) {
		super(props);
		this.state = store.getState();
	}

	componentDidMount = () => {
		// Subscribe to the store for updates
		this.unsubscribe = store.subscribe(this.storeDidUpdate);
	};

	componentWillUnmount = () => {
		// Unsubscribe from the store when this component unmounts
		this.unsubscribe();
	};

	storeDidUpdate = () => {
		this.setState(store.getState());
	};
	
	getCompletedCount = (todos = []) => {
		return todos.filter(todo => !todo.completed).length;
	};

	render () {
		// This is solely here to deomnstrate that it is possible to pass props in through server rendering. 
		// This can be very useful when you are in control of the server code.
		let header_text = this.props.header || 'todo app';
		let count = this.getCompletedCount(this.state.todo_list);
		
		return (
			<React.Fragment>
				<header className='todo-app-header'>{ header_text }</header>
				<section className='todo-notepad'>
					<TodoInput />
					<TodoList {...this.state} />
					<TodoStatusBar count={count} />
				</section>
				<footer className='todo-app-footer'>
					<div>React & Redux Example</div>
				</footer>
			</React.Fragment>
		);
	}

}
