let uuid = 0;
let generate_uuid = () => uuid++;

export function addTodoReducer (todo_list, action) {
	// We should attempt to be pure and not modify current state at all times
	return todo_list.concat({
		id: generate_uuid(),
		text: action.payload,
		completed: false
	});
}

export function toggleAllTodoReducer (todo_list, action) {
	return todo_list.map(todo => {
		todo.completed = action.payload;
		return todo;
	});
}
