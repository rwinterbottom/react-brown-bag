import { ADD_TODO, TOGGLE_ALL_TODOS } from './constants';

export function addTodo (text) {
	return {
		type: ADD_TODO,
		payload: text
	};
}

export function toggleAllTodos (toggle_status) {
	return {
		type: TOGGLE_ALL_TODOS,
		payload: toggle_status
	};
}
