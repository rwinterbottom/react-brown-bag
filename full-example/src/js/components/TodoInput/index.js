import { addTodo, toggleAllTodos } from './actions';
import store from 'js/store';
import React from 'react';

export default class TodoInput extends React.Component {
	
	constructor (props) {
		super(props);
		this.state = { all_complete: false };
	}

	addNewTodo = event => {
		// Prevent the default form action
		event.preventDefault();
		// Parse the value from our input
		let element = this.refs.addtodo;
		// Dispatch our action and reset our form
		if (element.value) {
			store.dispatch(addTodo(element.value));
			event.target.reset();
		}
	};

	toggleAllTodos = () => {
		this.setState((prevState) => {
			store.dispatch(toggleAllTodos(!prevState.all_complete));
			return { all_complete: !prevState.all_complete }
		});
	};

	render () {
		return (
			<div className='add-todo-input flex-row'>
				<div className='svg-icon-container flex' onClick={this.toggleAllTodos}>
					<svg className='svg-icon pointer icon-arrow'>
						<use xlinkHref='#icon-arrow' />
					</svg>
				</div>
				<form onSubmit={this.addNewTodo} className='add-todo-form'>
					<input ref='addtodo' placeholder='Add a todo and hit enter (or return)' />
				</form>
			</div>
		);
	}

}
