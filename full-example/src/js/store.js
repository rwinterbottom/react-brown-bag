import { createStore, applyMiddleware } from 'redux';
import logger_middleware from './middleware/logger';
import async_middleware from './middleware/async';
import reducers from './reducers';

// Always add the async middleware to support simple async actions
let middleware = [ async_middleware ];

// Only add the logger if we are not in production
if (process.env.NODE_ENV !== 'production') {
	middleware.push(logger_middleware);
}

export default createStore(
	reducers,
	applyMiddleware.apply(null, middleware)
);
