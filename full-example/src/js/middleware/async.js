export default function async_middleware (api) {
	return next => action => {
		return typeof action === 'function'
			? action(api.dispatch)
			: next(action);
	};
};
