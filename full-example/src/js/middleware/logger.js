const STYLE = {
	action: 'font-weight:bold;',
	state: 'color:blue;'
};

export default function logger_middleware (api) {
	return next => action => {
		let next_state = next(action);
		console.log(`%c ${action.type}:`, STYLE.action, action);
		console.log(`%c Next State:`, STYLE.state, api.getState());
		return next_state;
	};
};
