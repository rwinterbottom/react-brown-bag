/**
 * @description Postcss loader configurations
 */

const autoprefixer = require('autoprefixer');

module.exports = {
	plugins: [ autoprefixer ]
};
