const compression = require('compression');
const express = require('express');

let app = express();

app.use(compression({ level: 9 }));
app.use(express.static('dist'));

app.listen(8080, () => console.log('App Listening on Port 8080'));