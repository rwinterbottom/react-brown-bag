const ReactPrerenderPlugin = require('./plugins/react-prerender-plugin');
const InlineStylePlugin = require('./plugins/inline-style-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const path = require('path');

const root = process.cwd();

let index_entry = path.join(root, 'src/js/index'),
		output_path = path.join(root, 'dist'),
		webpack_config = {},
		plugins = [];

let make_config = (options = {}) => {

	let { precompile } = options;

	// Setup some default module rules for all environments
	let module_rules = [{
		test: /\.js?$/,
		loader: 'babel-loader'
	}];

	// Setup our node environment for building
	let node_environment = {
		'process.env': {
			'NODE_ENV': `"${process.env.NODE_ENV}"`,
			'SERVER_SIDE_RENDERING': process.env.SERVER_SIDE_RENDERING
		}
	};

	// Setup webpack to support hot reloading and our dev css module rules
	if (process.env.NODE_ENV === 'development') {
		// Add specific plugins and configurations for the development server
		plugins = [
			new webpack.NamedModulesPlugin(),
			new webpack.HotModuleReplacementPlugin()
		];

		// Update the entry for HMR
		index_entry = [
			`webpack-dev-server/client?http://localhost:${ process.env.PORT || 8080 }`,
			'webpack/hot/dev-server',
			index_entry
		];

		webpack_config.devServer = {
			contentBase: output_path,
			stats: {
				performance: true,
				modules: false,
				colors: true,
				assets: true
			},
			hot: true
		};

		// Add module rules for loading css
		module_rules.push({
			test: /\.s?css?$/,
			loaders: [ 'style-loader', 'css-loader', 'sass-loader' ]
		});
	}

	// If we are procompiling a component, add options here
	else if (precompile) {
		// We need this loader incase the app imports scss, but cannot use
		// style-loader as it will break when we attempt to require the component
		module_rules.push({
			test: /\.s?css?$/,
			loaders: [ 'css-loader', 'sass-loader' ]
		});
	}

	// Add production plugins and module rules
	else {
		// Add production specific plugins for css and uglifying js
		plugins = [
			new ExtractTextPlugin('css/critical.css'),
		  new InlineStylePlugin({
				styles_path: 'css/critical.css',
				name: 'critical'
			}),
			new ReactPrerenderPlugin({
				component_path: path.join(output_path, 'js/app.compiled'),
				name: 'markup'
			}),
			new webpack.optimize.UglifyJsPlugin({
				mangle: { screw_ie8: true },
				compressor: {
					screw_ie8: true,
					warnings: false
				},
				output: {
					screw_ie8: true,
					comments: false
				}
			})
		];

		// Add module rules for css that are more specific for a production environment
		// This is for our critical css
		module_rules.push({
			test: /critical\.s?css?$/,
			loaders: ExtractTextPlugin.extract([ 'css-loader', 'postcss-loader', 'sass-loader' ])
		});

		// This is for css that can be lazy loaded
		module_rules.push({
			test: /app\.s?css?$/,
			loaders: [ 'style-loader', 'css-loader', 'postcss-loader', 'sass-loader' ]
		});

	}

	// Combine all the necessary plugins
	plugins = plugins.concat([
		new webpack.DefinePlugin(node_environment),
		new webpack.optimize.OccurrenceOrderPlugin()
	]);

	// The only time these are not needed is during a precompile step
	if (!precompile) {
		plugins.push(new webpack.optimize.CommonsChunkPlugin({ name: 'vendor', minChunks: 2 }));
		plugins.push(new HtmlWebpackPlugin({
			template: path.join(root, 'src/index.html'),
			inject: false,
			minify: {
				removeStyleLinkTypeAttributes: true,
				removeRedundantAttributes: true,
				removeEmptyAttributes: true,
				collapseWhitespace: true,
				removeComments: true,
				minifyURLs: true,
				minifyCSS: true,
				minifyJS: true
			}
		}));
	}

	// Define our webpack_config
	webpack_config = Object.assign(webpack_config, {
		profile: true,
		entry: {
			index: index_entry,
			vendor: ['react', 'react-dom']
		},
		output: {
			path: output_path,
			filename: 'js/[name].[hash].js'
		},
		resolve: {
			alias: {
				js: path.join(root, 'src/js'),
				css: path.join(root, 'src/css')
			}
		},
		module: { rules: module_rules },
		plugins: plugins
	});

	return webpack_config;

};

module.exports = make_config;
